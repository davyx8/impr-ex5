function [image] = IDWT(waveletDecomp, lowFilt, highFilt, levels)

highFilt=fliplr(2*highFilt);
lowFilt=lowFilt*2;

[y x]=size(waveletDecomp);
image = waveletDecomp;
LL = waveletDecomp;

y = y / 2^levels
x = x / 2^levels

for i=1:levels
    i
    LL = image(1:y, 1:x);
    size(LL)
    LH = image(1:y, (x+1):x*2);
    HL = image((y+1):(y*2), 1:x);
    HH = image((y+1):(y*2), (x+1):x*2);
    size(image)
    tmp=zeros(2*y,x);
    tmp(2:2:end,:)=LL;
    LL = conv2(tmp, lowFilt', 'same');
    
    tmp = zeros(y*2, x);
    tmp(2:2:end, :) = LH;
    LH = conv2(tmp, highFilt', 'same');
    L = LL + LH;
    tmp = zeros(y*2, x*2);
    tmp(:,2:2:end) = L;
    L = conv2(tmp, lowFilt, 'same');
    
    tmp = zeros(y*2, x);
    tmp(2:2:end, :) = HL;
    HL = conv2(tmp, lowFilt', 'same');
    
    tmp = zeros(y*2, x);
    tmp(2:2:end, :) = HH;
    HH = conv2(tmp, highFilt', 'same');
    H = HH + HL;
    tmp = zeros(y*2, x*2);
    tmp(:,2:2:end) = H;
    H = conv2(tmp, highFilt, 'same');
    image(1:(y*2), 1:(x*2)) = L + H;
    
    x=x*2;
    y=y*2;
    
end

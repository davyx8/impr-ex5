function [waveletDecomp] = DWT(image, lowFilt, highFilt, levels)
[x y]=size(image)
waveletDecomp=zeros(x,y);
LL = image;

for i=1:levels
    L = conv2(LL,lowFilt,'same');
    L = L(:,1:2:end);
    H = conv2(LL,highFilt,'same');
    H = H(:,1:2:end);
    LL = conv2(L, lowFilt', 'same');
    LL = LL(1:2:end, :);
    LH = conv2(L, highFilt', 'same');
    LH = LH(1:2:end, :);
    
    HL = conv2(H, lowFilt', 'same');
    HL = HL(1:2:end, :);
    HH = conv2(H, highFilt', 'same');
    HH = HH(1:2:end, :);
    
    
    waveletDecomp(1:x/2, (y/2+1):y) = LH;
    waveletDecomp((x/2+1):x, 1:y/2) = HL;
    waveletDecomp((x/2+1):x, ((y/2)+1):y) = HH;
    x = x / 2;
    y = y / 2;
end

waveletDecomp(1:x, 1:y) = LL;



end

function [imQuant, error] = quantizeImage(imOrig, nQuant, nIter)

if (ndims(imOrig) == 3)
    imYIQ = transformRGB2YIQ(imOrig);
    im = imYIQ(:,:,1);
    x = imYIQ(:,:,1);
else
    im = (imOrig);
    x=im;
end

% Z init.
pixnum=numel(im);
hist=imhist(im);
cHist = cumsum(hist);
cHist = round((cHist ./ max(cHist) .* (nQuant - 1)));
Z = [0; find(diff(cHist)); 255];
q = zeros(1, nQuant);
error=zeros(1, nIter);
for i = 1 : nIter
    for j = 1 : nQuant
        currHist = hist((Z(j)+1):Z(j+1));
        q(j) = sum((currHist).*(1+Z(j):Z(j+1))')/sum(currHist);
        im(im>Z(j)/255 & im<=Z(j+1)/255) = q(j)/255;
    end
    
    for r = 2 : nQuant
        Z(r) = floor((q(r-1) + q(r) )/2 ) ;
    end
    error(i) = (sum(sum(abs(x-im))));
    if i == nIter
        continue;
    end
    if (ndims(imOrig) == 3)
        im=imYIQ(:,:,1);
    else
        im= (imOrig);
    end
end
LookUp = zeros(1, 255);
for i = 1:nQuant
    LookUp(Z(i)+1:Z(i+1)) = q(i);
end

im=im2uint8(im);
imQuant=LookUp(im+1)/255;

if (ndims(imOrig) == 3)
    imYIQ(:,:,1) = imQuant;
    imQuant=transformYIQ2RGB(imYIQ);
end
% figure
% subplot(1,3,1);
% imshow(imOrig);
% subplot(1,3,2);
% imshow(imQuant);
% subplot(1,3,3);
% plot(error);

end


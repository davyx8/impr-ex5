function [outImage] = deleteHorizontal(image, lowFilt, highFilt, levels)
[y, x] = size(image);

%[dx dy]=gradientxy(image);
y=y/2;
waveletDecomp = DWT(image, lowFilt, highFilt, levels);
for i=1:levels
    waveletDecomp(1:y,x/2+1:x)=0;
    y=y/2;
    x=x/2;
    
end
outImage = IDWT(waveletDecomp,lowFilt,highFilt,levels);
figure;
imshow(outImage);
end
function [compressedImage, waveletDecompCompressed] =  waveletQuantization(image, lowFilt, highFilt, levels, nQuant)
waveletDecomp = DWT(image, lowFilt, highFilt, levels);
[y, x] = size(image);
waveletDecompCompressed = zeros(y,x);

y = y/2^levels;
x = x/2^levels;

LL = waveletDecomp(1:y,1:x);
mask = true(size(waveletDecomp));
mask(1:y,1:x)=false;

tmp = waveletDecomp(mask);
minimun = min(tmp(:));
tmp = tmp -minimun;

compressed = quantizeImage(tmp,nQuant,8);
compressed = compressed +minimun;
waveletDecompCompressed(mask) = compressed;


waveletDecompCompressed(1:y,1:x)=LL;
compressedImage = IDWT(waveletDecompCompressed,lowFilt,highFilt,levels);


waveletDecomp(mask) = waveletDecomp(mask)+0.5;
waveletDecomp = im2uint8(waveletDecomp);
waveletDecompCompressed(mask) = waveletDecompCompressed(mask) +0.5;
waveletDecompCompressed = im2uint8(waveletDecompCompressed);




save('beforeCompress.mat','waveletDecomp','-v6');
gzip('beforeCompress.mat');
save('afterCompress.mat','waveletDecompCompressed','-v6');
gzip('afterCompress.mat');
figure;
imshow(compressedImage);
title('Compressed Image');


end


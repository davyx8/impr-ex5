function denoisImage=  denoising(image,lowFilt,highFilt,levels )
[y, x] = size(image);
t=0.042;
%[dx dy]=gradientxy(image);
maskingTape=ones(y,x);
waveletDecomp = DWT(image, lowFilt, highFilt, levels);
y=y/2^levels;
x=x/2^levels;
maskingTape(1:y,1:x)=0;
waveletDecomp(abs(waveletDecomp)<t & maskingTape==1 )=0;
denoisImage = IDWT(waveletDecomp,lowFilt,highFilt,levels);
figure;
imshow(denoisImage);
end